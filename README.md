# ACL Anthology Download

Command line tool to download and convert ACL Anthology .bib data and PDFs. 

This tool:

1. Downloads a current ACL Anthology .bib file
2. Creates a separate bibliography for each entry type (.bib and XML formats)
3. Downloads all PDFs associated with papers in the collection
4. Separates PDFs larger than a given number of pages (by default, 50) into sub-directories  

### Requirements
* bash
* make
* [bibtool](http://www.gerd-neugebauer.de/software/TeX/BibTool/en/)
* pdfinfo (from xpdf)
* [pybtex](https://pybtex.org/) -- in your python library (e.g., install via `pip install pybtex`)
* python3

### Installation and Usage

Once you have installed the requirements above, install the package by issuing:

```
git clone https://gitlab.com/dprl/acl-anthology-download.git
```

To run the tool, then issue:

```
make
```

Bibliography and PDF files will be located in the `bib/` and `pdf` directories, respectively. Some additional files for book keeping are also created, which are identified in command line messages. 

The final step is to separate long files into separate subdirectories. The whole process makes use of two scripts (i.e., `compile_acl_data` and `separate_long_docs`).


### Cleaning Up

To remove all downloaded and generated data, you can issue:

```
make clean
```

The main script will also ask whether to remove previously downloaded and generated data if the script was run previously. This avoids accidentally deleting everything when calling the script again.

## Modifying the Program

There are parameters that can be modified at the top of the `compile_acl_data` file:

* `BIB` -- the bibliography file to download (with/without abstracts; default without)
* `INDEX_TYPES` -- the BibTeX entry types to compile data for
* `WGET_P` -- number of processes used for parallel `wget` calls when downloading data in the script.

This parameter can be modified at the top of the `separate_long_docs` script:

* `MAX_PAGES` -- maximum number of pages that a document can be without being moved into a `long/` subdirectory.

## Support 

This material is based upon work supported by the National Science Foundation (USA) under Grant Nos. IIS-1016815, IIS-1717997, and 2019897 (MMLI), and the Alfred P. Sloan Foundation under Grant No. G-2017-9827.
Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation or the Alfred P. Sloan Foundation.

## Authors

Richard Zanibbi (Rochester Institute of Technology, USA), May 2022
