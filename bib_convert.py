################################################################
# bib_convert.py
#
# Extract metadata from .bib file using the pybtex library
#
# R. Zanibbi, May 2022
################################################################

from pybtex.database import parse_file
from pybtex.database import BibliographyData 
import sys
import os


##################################
# Main function
##################################
# HACK: First argument is the file to process
if __name__ == "__main__":
    inFile = sys.argv[1]
    outFile = os.path.splitext( inFile )[0] + ".xml"
    
    bib_data = parse_file(inFile)
    bib_data.to_file( outFile, bib_format="bibtexml")

    # FOR LATER REFERENCE:
    # Process each file
    #for bib_id in bibdata.entries:
    #    bibfields = bibdata.entries[bib_id].fields
    #    try:
    #        print(bibfields["pages"])
        
    #    except:
    #        print("Warning: error for entry ",bib_id)
    #        continue
