default:
	./compile_acl_data
	./separate_long_docs ./pdf/@article
	./separate_long_docs ./pdf/@inproceedings
clean:
	rm -f *.bib *.gz *.txt
	rm -rf pdf/ bib/
